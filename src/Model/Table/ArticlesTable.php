<?php

/**
 * Created by IntelliJ IDEA.
 * User: boonhong
 * Date: 1/26/16
 * Time: 3:18 PM
 */
namespace App\Model\Table;

use Cake\ORM\Table;

class ArticlesTable extends Table
{
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');
    }
}
