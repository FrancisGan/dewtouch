<?php
/**
 * Created by IntelliJ IDEA.
 * User: boonhong
 * Date: 1/26/16
 * Time: 3:25 PM
 */

namespace App\Controller;


class ArticlesController extends AppController
{
    public function index()
    {
        $this->set('articles', $this->Articles->find('all'));
    }
}
